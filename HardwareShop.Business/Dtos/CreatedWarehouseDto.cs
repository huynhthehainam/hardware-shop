﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareShop.Business.Dtos
{
    public class CreatedWarehouseDto
    {
        public int Id { get; set; }
    }
}
