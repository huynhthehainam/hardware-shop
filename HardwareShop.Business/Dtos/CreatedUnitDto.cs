

namespace HardwareShop.Business.Dtos
{
    public class CreatedUnitDto
    {
        public int Id { get; set; }
    }
}