

namespace HardwareShop.Business.Dtos
{
    public class CreatedNotificationDto
    {
        public Guid Id { get; set; }
    }
}