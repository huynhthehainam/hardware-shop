﻿using HardwareShop.Business.Implementations;
using HardwareShop.Business.Services;
using HardwareShop.Dal.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace HardwareShop.Business.Extensions
{
    public static class BusinessExtension
    {
        public static void ConfigureBusiness(this IServiceCollection services)
        {
            services.ConfigureRepository();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IShopService, ShopService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<ICustomerDebtService, CustomerDebtService>();
            services.AddScoped<IProductCategoryService, ProductCategoryService>();
            services.AddScoped<IUnitService, UnitService>();
            services.AddScoped<IUnitCategoryService, UnitCategoryService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IAssetService, AssetService>();
        }
    }
}
